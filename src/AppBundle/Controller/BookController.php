<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\Category;
use AppBundle\Logic\Promotions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Intl;
use Doctrine\Common\Collections\Criteria;

/**
 * Books controller.
 *
 * @Route("")
 */
class BookController extends Controller
{
    private $manager;

    /**
     * Lists all books entities.
     *
     * @Route("/", name="book_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getManager();

        $books = $em->getRepository('AppBundle:Book')->findBy(['visible' => true], ['sort' => 'DESC', 'id' => 'DESC']);
        $categories = $em->getRepository('AppBundle:Category')->findAll();

        foreach ($books as $book) {
            $book = Promotions::apply_promotion($em, $this->getUser(), $book);
        }

        return $this->render('books/index.html.twig', array(
            'books' => $books,
            'menu' => 'book',
            'sel_cat' => 0,
            'page_title' => 'Всички книги',
            'user' => $this->getUser(),
            'categories' => $categories,
            'show_sidebar' => true
        ));
    }

    /**
     * Lists books entities filtered by category.
     *
     * @Route("/book/category/{id}", name="book_category")
     * @ParamConverter("category")
     * @Method("GET")
     */
    public function categoryAction(Category $category) {

        $em = $this->getManager();
        $books = $category->getBooks(['sort' => 'DESC', 'id' => 'DESC']);
        $categories = $em->getRepository('AppBundle:Category')->findAll();

        foreach ($books as $book) {
            $book = Promotions::apply_promotion($em, $this->getUser(), $book);
        }

        return $this->render('books/index.html.twig', array(
            'books' => $books,
            'menu' => 'book',
            'sel_cat' => $category->getId(),
            'page_title' => 'Книги в категория ' . $category->getName(),
            'user' => $this->getUser(),
            'categories' => $categories,
            'show_sidebar' => true
        ));
    }

    /**
     * View book
     *
     * @Route("/book/view/{id}", name="book_view")
     * @ParamConverter("book")
     * @Method("GET")
     */
    public function viewAction(Book $book)
    {
        if (!$book->isVisible()) {
            return $this->redirect('/');
        }

        $em = $this->getManager();

        $book = Promotions::apply_promotion($em, $this->getUser(), $book);

        $secondhand = $book->getBooksUser();

        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq("selling", "1"));

        $secondhand = $secondhand->matching($criteria);

        $categories = $em->getRepository('AppBundle:Category')->findAll();

        return $this->render('books/view.html.twig', array(
            'book' => $book,
            'secondhand' => $secondhand,
            'menu' => 'book',
            'sel_cat' => $book->getCategory()->getId(),
            'user' => $this->getUser(),
            'categories' => $categories,
            'white_page' => true,
            'show_sidebar' => true
        ));
    }

    private function getManager()
    {
        if (null === $this->manager)
            $this->manager = $this->getDoctrine()->getManager();

        return $this->manager;
    }
}
