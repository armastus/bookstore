<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\Category;
use AppBundle\Entity\User;
use AppBundle\Entity\UsersBook;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Intl;

/**
 * Manage controller.
 *
 * @Route("profile")
 */
class ProfileController extends Controller
{
    /**
     * Lists all UsersBooks entities.
     *
     * @Route("/mybooks", name="profile_mybooks")
     * @Method("GET")
     */
    public function myBooksAction()
    {
        $usersbooks = $this->getUser()->getBooksUser();

        return $this->render('profile/mybooks.html.twig', array(
            'usersbooks'  => $usersbooks,
            'user'      => $this->getUser(),
            'menu' => 'mybooks',
            'page_title' => 'Моите книги',
            'single_page' => true
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/mybooks/{id}/edit", name="profile_mybooks_edit")
     * @Method({"GET", "POST"})
     */
    public function myBookEditAction(Request $request, UsersBook $usersbook)
    {
        if ($usersbook->getUser()->getId() != $this->getUser()->getId()) {
            $this->get('session')->getFlashBag()->add('error', 'Вие не сте притежател на тази книга');

            return $this->redirectToRoute('profile_mybooks');
        }

        $editForm   = $this->createForm('AppBundle\Form\UsersBookType', $usersbook, ['access' => 'limited']);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Книгата беше редактирана успешно');
            $user_id = $usersbook->getUser()->getId();
            return $this->redirectToRoute('profile_mybooks');
        }

        return $this->render('forms/form.html.twig', array(
            'user'      => $this->getUser(),
            'form'   => $editForm->createView(),
            'menu' => 'mybooks',
            'page_title' => 'Редакция на книга ' . $usersbook->getBook()->getTitle(),
            'single_page' => true,
            'btn_name' => 'Редактиране'
        ));
    }

    /**
     * Displays a form to edit user profile.
     *
     * @Route("/info", name="profile_info")
     * @Method({"GET", "POST"})
     */
    public function editUserAction(Request $request)
    {
        $user = $this->getUser();
        $options = ['access' => 'user_profile'];
        $editForm   = $this->createForm('AppBundle\Form\UserType', $user, $options);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $user->setUpdatedAt(new \DateTime());

            $encoder = $this->get('security.password_encoder');

            $user->setPassword(
                $encoder->encodePassword($user, $user->getPasswordRaw())
            );

            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Профилът беше обновен успешно');
        }

        return $this->render('forms/profile.html.twig', array(
            'user'      => $this->getUser(),
            'form'   => $editForm->createView(),
            'menu' => 'profile',
            'page_title' => 'Профил',
            'single_page' => true,
            'btn_name' => 'Редактиране',
        ));
    }
}
