<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\Category;
use AppBundle\Entity\Promotion;
use AppBundle\Entity\User;
use AppBundle\Entity\UsersBook;
use AppBundle\Logic\Promotions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Intl;

/**
 * Manage controller.
 *
 * @Route("manage")
 */
class ManageController extends Controller
{
    /**
     * Lists all Category entities.
     *
     * @Route("/categories", name="manage_categories")
     * @Method("GET")
     */
    public function viewCategoriesAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('AppBundle:Category')->findAll();

        return $this->render('manage/categories.html.twig', array(
            'categories'  => $categories,
            'user'      => $this->getUser(),
            'menu' => 'manage',
            'page_title' => 'Категории',
            'single_page' => true,
            'admin_navigation' => true,
            'submenu' => 'categories'
        ));
    }

    /**
     * Creates a new category entity.
     *
     * @Route("/categories/new", name="manage_category_new")
     * @Method({"GET", "POST"})
     */
    public function newCategoryAction(Request $request)
    {
        $category = new Category();
        $form    = $this->createForm('AppBundle\Form\CategoryType', $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $category->setCreatedAt(new \DateTime());
            $category->setUpdatedAt(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Категорията беше създадена успешно!');

            return $this->redirectToRoute('manage_categories');
        }

        return $this->render('forms/form.html.twig', array(
            'user'      => $this->getUser(),
            'form'    => $form->createView(),
            'menu' => 'manage',
            'page_title' => 'Създаване на категория',
            'single_page' => true,
            'btn_name' => 'Създаване',
            'admin_navigation' => true,
            'submenu' => 'categories'
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     * @Route("/categories/{id}/edit", name="manage_category_edit")
     * @Method({"GET", "POST"})
     */
    public function editCategoryAction(Request $request, Category $category)
    {

        if (!$this->isGranted('ROLE_ADMIN', $this->getUser())) {
            $this->get('session')->getFlashBag()->add('error', 'Нямате достатъчно права');

            return $this->redirectToRoute('manage_categories');
        }

        $editForm   = $this->createForm('AppBundle\Form\CategoryType', $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $category->setUpdatedAt(new \DateTime());
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Категорията беше редактирана успешно');

            return $this->redirectToRoute('manage_categories');
        }

        return $this->render('forms/form.html.twig', array(
            'user'      => $this->getUser(),
            'form'   => $editForm->createView(),
            'menu' => 'manage',
            'page_title' => 'Редакция на категория',
            'single_page' => true,
            'btn_name' => 'Редактиране',
            'admin_navigation' => true,
            'submenu' => 'categories'
        ));
    }

    /**
     * Deletes a category entity.
     *
     * @Route("/categories/{id}/delete", name="manage_category_delete")
     * @Method("GET")
     */
    public function deleteCategoryAction(Request $request, Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($category);
        $em->flush();

        return $this->redirectToRoute('manage_categories');
    }


    /**
     * Lists all Book entities.
     *
     * @Route("/books", name="manage_books")
     * @Method("GET")
     */
    public function viewBooksAction()
    {
        $em = $this->getDoctrine()->getManager();

        $books = $em->getRepository('AppBundle:Book')->findAll();

        return $this->render('manage/books.html.twig', array(
            'books'  => $books,
            'user'      => $this->getUser(),
            'menu' => 'manage',
            'page_title' => 'Книги',
            'single_page' => true,
            'admin_navigation' => true,
            'submenu' => 'books'
        ));
    }

    /**
     * Creates a new book entity.
     *
     * @Route("/books/new", name="manage_book_new")
     * @Method({"GET", "POST"})
     */
    public function newBookAction(Request $request)
    {
        $book = new Book();
        $form    = $this->createForm('AppBundle\Form\BookType', $book);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $book->setCreatedAt(new \DateTime());
            $book->setUpdatedAt(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($book);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Книгата беше създадена успешно!');

            return $this->redirectToRoute('manage_books');
        }

        return $this->render('forms/form.html.twig', array(
            'user'      => $this->getUser(),
            'form'    => $form->createView(),
            'menu' => 'manage',
            'page_title' => 'Създаване на книга',
            'single_page' => true,
            'btn_name' => 'Създаване',
            'admin_navigation' => true,
            'submenu' => 'books'
        ));
    }

    /**
     * Displays a form to edit an existing book entity.
     *
     * @Route("/books/{id}/edit", name="manage_book_edit")
     * @Method({"GET", "POST"})
     */
    public function editBookAction(Request $request, Book $book)
    {

        $editForm   = $this->createForm('AppBundle\Form\BookType', $book,
            ['access' => $this->getUser()->getRole() == 'ROLE_EDITOR' ? 'limited' : 'full']);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $book->setUpdatedAt(new \DateTime());
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Книгата беше редактирана успешно');

            return $this->redirectToRoute('manage_books');
        }

        return $this->render('forms/form.html.twig', array(
            'user'      => $this->getUser(),
            'form'   => $editForm->createView(),
            'menu' => 'manage',
            'page_title' => 'Редакция на книга',
            'single_page' => true,
            'btn_name' => 'Редактиране',
            'admin_navigation' => true,
            'submenu' => 'books'
        ));
    }

    /**
     * Deletes a book entity.
     *
     * @Route("/books/{id}/delete", name="manage_book_delete")
     * @Method("GET")
     */
    public function deleteBookAction(Request $request, Book $book)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($book);
        $em->flush();

        return $this->redirectToRoute('manage_books');
    }


    /**
     * Lists all User entities.
     *
     * @Route("/users", name="manage_users")
     * @Method("GET")
     */
    public function viewUsersAction()
    {
        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('AppBundle:User')->findAll();

        return $this->render('manage/users.html.twig', array(
            'users'  => $users,
            'user'      => $this->getUser(),
            'menu' => 'manage',
            'page_title' => 'Потребители',
            'single_page' => true,
            'admin_navigation' => true,
            'submenu' => 'users'
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/users/new", name="manage_user_new")
     * @Method({"GET", "POST"})
     */
    public function newUserAction(Request $request)
    {
        $user = new User();
        $form    = $this->createForm('AppBundle\Form\UserType', $user, ['access' => 'full']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setCreatedAt(new \DateTime());
            $user->setUpdatedAt(new \DateTime());

            $encoder = $this->get('security.password_encoder');

            $user->setPassword(
                $encoder->encodePassword($user, $user->getPasswordRaw())
            );

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Потребителят беше създаден успешно!');

            return $this->redirectToRoute('manage_users');
        }

        return $this->render('forms/form.html.twig', array(
            'user'      => $this->getUser(),
            'form'    => $form->createView(),
            'menu' => 'manage',
            'page_title' => 'Създаване на потребител',
            'single_page' => true,
            'btn_name' => 'Създаване',
            'admin_navigation' => true,
            'submenu' => 'users'
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/users/{id}/edit", name="manage_user_edit")
     * @Method({"GET", "POST"})
     */
    public function editUserAction(Request $request, User $user)
    {
        $options = ['access' => 'full'];
        if($user->getId() == $this->getUser()->getId())
        {
            $options['hide_role_field'] = true;
        }
        $editForm   = $this->createForm('AppBundle\Form\UserType', $user, $options);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $user->setUpdatedAt(new \DateTime());

            $encoder = $this->get('security.password_encoder');

            $user->setPassword(
                $encoder->encodePassword($user, $user->getPasswordRaw())
            );

            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Потребителят беше редактиран успешно');

            return $this->redirectToRoute('manage_users');
        }

        return $this->render('forms/form.html.twig', array(
            'user'      => $this->getUser(),
            'form'   => $editForm->createView(),
            'menu' => 'manage',
            'page_title' => 'Редакция на потребител',
            'single_page' => true,
            'btn_name' => 'Редактиране',
            'admin_navigation' => true,
            'submenu' => 'users'
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/users/{id}/delete", name="manage_user_delete")
     * @Method("GET")
     */
    public function deleteUserAction(Request $request, User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($user);
        $em->flush();

        return $this->redirectToRoute('manage_users');
    }


    /**
     * Lists all UsersBooks entities.
     *
     * @Route("/usersbooks/{id}/view", name="manage_usersbooks")
     * @Method("GET")
     */
    public function viewUsersBooksAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();

        $usersbooks = $user->getBooksUser();

        return $this->render('manage/usersbooks.html.twig', array(
            'usersbooks'  => $usersbooks,
            'edited_user' => $user->getId(),
            'user'      => $this->getUser(),
            'menu' => 'manage',
            'page_title' => 'Притежания на ' . $user->getUsername(),
            'single_page' => true,
            'admin_navigation' => true,
            'submenu' => 'users'
        ));
    }

    /**
     * Creates a new user entity.
     *
     * @Route("/usersbooks/{id}/new", name="manage_usersbook_new")
     * @Method({"GET", "POST"})
     */
    public function newUsersBookAction(Request $request, User $user)
    {
        $usersbook = new UsersBook();
        $form    = $this->createForm('AppBundle\Form\UsersBookType', $usersbook);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $usersbook->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($usersbook);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Притежанието беше създадено успешно!');

            $user_id = $usersbook->getUser()->getId();
            return $this->redirectToRoute('manage_usersbooks', ['id' => $user_id]);
        }

        return $this->render('forms/form.html.twig', array(
            'user'      => $this->getUser(),
            'form'    => $form->createView(),
            'menu' => 'manage',
            'page_title' => 'Създаване на притежание',
            'single_page' => true,
            'btn_name' => 'Създаване',
            'admin_navigation' => true,
            'submenu' => 'users'
        ));
    }

    /**
     * Displays a form to edit an existing user entity.
     *
     * @Route("/usersbooks/{id}/edit", name="manage_usersbook_edit")
     * @Method({"GET", "POST"})
     */
    public function editUsersBookAction(Request $request, UsersBook $usersbook)
    {
        $editForm   = $this->createForm('AppBundle\Form\UsersBookType', $usersbook);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Притежанието беше редактирано успешно');
            $user_id = $usersbook->getUser()->getId();
            return $this->redirectToRoute('manage_usersbooks', ['id' => $user_id]);
        }

        return $this->render('forms/form.html.twig', array(
            'user'      => $this->getUser(),
            'form'   => $editForm->createView(),
            'menu' => 'manage',
            'page_title' => 'Редакция на притежание',
            'single_page' => true,
            'btn_name' => 'Редактиране',
            'admin_navigation' => true,
            'submenu' => 'users'
        ));
    }

    /**
     * Deletes a user entity.
     *
     * @Route("/usersbooks/{id}/delete", name="manage_usersbook_delete")
     * @Method("GET")
     */
    public function deleteUsersBookAction(Request $request, UsersBook $usersbook)
    {
        $user_id = $usersbook->getUser()->getId();
        $em = $this->getDoctrine()->getManager();
        $em->remove($usersbook);
        $em->flush();

        return $this->redirectToRoute('manage_usersbooks', ['id' => $user_id]);
    }


    /**
     * Lists all Promotion entities.
     *
     * @Route("/promotions", name="manage_promotions")
     * @Method("GET")
     */
    public function viewPromotionsAction()
    {
        $em = $this->getDoctrine()->getManager();

        $promotions = $em->getRepository('AppBundle:Promotion')->findAll();

        return $this->render('manage/promotions.html.twig', array(
            'promotions'  => $promotions,
            'user'      => $this->getUser(),
            'menu' => 'manage',
            'page_title' => 'Книги',
            'single_page' => true,
            'admin_navigation' => true,
            'submenu' => 'promotions',
            'promotion_types' => Promotions::$active_types
        ));
    }

    /**
     * Creates a new promotion entity.
     *
     * @Route("/promotions/new/{type}", name="manage_promotion_new")
     * @Method({"GET", "POST"})
     */
    public function newPromotionAction(Request $request, $type = 'general')
    {
        if( ! in_array($type, array_keys(Promotions::$active_types)))
        {
            return $this->redirectToRoute('manage_promotions');
        }
        $promotion = new Promotion();
        $form    = $this->createForm('AppBundle\Form\PromotionType', $promotion, ['type' => $type]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $promotion->setCreatedAt(new \DateTime());
            $promotion->setUpdatedAt(new \DateTime());

            $promotion->setType($type);

            $em = $this->getDoctrine()->getManager();
            $em->persist($promotion);
            $em->flush();

            $this->get('session')->getFlashBag()->add('success', 'Промоцията беше създадена успешно!');

            return $this->redirectToRoute('manage_promotions');
        }

        return $this->render('forms/form.html.twig', array(
            'user'      => $this->getUser(),
            'form'    => $form->createView(),
            'menu' => 'manage',
            'page_title' => 'Създаване на промоция',
            'single_page' => true,
            'btn_name' => 'Създаване',
            'admin_navigation' => true,
            'submenu' => 'promotions'
        ));
    }

    /**
     * Displays a form to edit an existing promotion entity.
     *
     * @Route("/promotions/{id}/edit", name="manage_promotion_edit")
     * @Method({"GET", "POST"})
     */
    public function editPromotionAction(Request $request, Promotion $promotion)
    {

        $editForm   = $this->createForm('AppBundle\Form\PromotionType', $promotion, ['type' => $promotion->getType()]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $promotion->setUpdatedAt(new \DateTime());
            $this->getDoctrine()->getManager()->flush();

            $this->get('session')->getFlashBag()->add('success', 'Промоцията беше редактирана успешно');

            return $this->redirectToRoute('manage_promotions');
        }

        return $this->render('forms/form.html.twig', array(
            'user'      => $this->getUser(),
            'form'   => $editForm->createView(),
            'menu' => 'manage',
            'page_title' => 'Редакция на промоция',
            'single_page' => true,
            'btn_name' => 'Редактиране',
            'admin_navigation' => true,
            'submenu' => 'promotions'
        ));
    }

    /**
     * Deletes a promotion entity.
     *
     * @Route("/promotions/{id}/delete", name="manage_promotion_delete")
     * @Method("GET")
     */
    public function deletePromotionAction(Request $request, Promotion $promotion)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($promotion);
        $em->flush();

        return $this->redirectToRoute('manage_promotions');
    }


}
