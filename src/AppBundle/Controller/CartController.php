<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Book;
use AppBundle\Entity\Category;
use AppBundle\Entity\UsersBook;
use AppBundle\Logic\Promotions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Intl\Intl;


/**
 * Carts controller.
 *
 * @Route("cart")
 */
class CartController extends Controller
{
    private $manager;

    /**
     * Lists all carts entities.
     *
     * @Route("/", name="cart_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getManager();
        $books = $em->getRepository('AppBundle:Cart')->findAll();

        foreach ($books as $book) {
            $book = Promotions::apply_promotion($em, $this->getUser(), $book);
        }

        return $this->render('carts/index.html.twig', array(
            'books' => $books,
            'menu' => 'cart',
            'sel_cat' => 0,
            'page_title' => 'Всички книги',
            'user' => $this->getUser(),

        ));
    }

    private function getManager()
    {
        if (null === $this->manager)
            $this->manager = $this->getDoctrine()->getManager();

        return $this->manager;
    }

    /**
     * Add book to cart.
     *
     * @Route("/add/{id}/{usersbook_id}", name="cart_add", defaults={"usersbook_id" = 0})
     * @ParamConverter("book")
     * @Method("GET")
     */
    public function addAction(Request $request, Book $book, $usersbook_id = 0)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $cart = $session->get('cart', []);
        $bookId = $book->getId();
        $usersbook = $usersbook_id > 0
            ? $em->getRepository('AppBundle:UsersBook')
                ->findOneBy(['id' => $usersbook_id, 'selling' => true])
            : null;
        $usersBookId = $usersbook ? $usersbook->getId() : 0;
        if (!$book->isVisible()) {
            $session->getFlashBag()->add('error', 'Продуктът не е наличен');
        } else {
            $key = $bookId . '_' . $usersBookId;
            $qty = ($cart[$key]['qty'] ?? 0) + 1;
            $item = [$key => ['qty' => $qty, 'bookId' => $bookId, 'userBookId' => $usersBookId]];
            if ($this->validate_quantity($item)) {
                $cart[$key] = $item[$key];
                $session->set('cart', $cart);
                $session->getFlashBag()->add('success', 'Продуктът беше успешно добавен');
            } else {
                $session->getFlashBag()->add('error', 'Недостатъчни количества');
            }
        }

        return $this->redirect('/cart/view');
    }

    /**
     * Update book quantity.
     *
     * @Route("/update/{key}/{qty}", name="cart_update")
     * @Method("GET")
     */
    public function updateAction(Request $request, $key, $qty)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $cart = $session->get('cart', []);
        $keydata = explode('_', $key);
        $bookId = $keydata[0];
        $usersBookId = $keydata[1];
        $item = [$key => ['qty' => $qty, 'bookId' => $bookId, 'userBookId' => $usersBookId]];
        if (isset($cart[$key]) && $qty > 0 && $qty <= 10 && $this->validate_quantity($item)) {
            $cart[$key] = $item[$key];
            $session->set('cart', $cart);
            $session->getFlashBag()->add('success', 'Количествата бяха успешно променени');
        } else {
            $session->getFlashBag()->add('error', 'Недостатъчни количества');
        }

        return $this->redirect('/cart/view');
    }

    /**
     * Remove book from cart.
     *
     * @Route("/remove/{key}", name="cart_remove")
     * @Method("GET")
     */
    public function removeAction(Request $request, $key)
    {
        $em = $this->getDoctrine()->getManager();
        $session = $request->getSession();
        $cart = $session->get('cart', []);
        if (isset($cart[$key])) {
            unset($cart[$key]);
            $session->set('cart', $cart);
        }

        return $this->redirect('/cart/view');
    }

    /**
     * View cart
     *
     * @Route("/view", name="cart_view")
     * @Method({"GET", "POST"})
     */
    public function viewAction(Request $request)
    {
        $em = $this->getManager();
        $session = $request->getSession();
        $cart = $session->get('cart', []);
        $status = [];

        if ($request->isMethod('POST')) {
            if (!$this->validate_quantity($cart)) {
                $session->getFlashBag()->add('error', 'Недостатъчни количества');
            } elseif (!$this->validate_cash($cart)) {
                $session->getFlashBag()->add('error', 'Нямате достатъчна наличност във виртуалния портфейл');
            } else {
                $total = 0;
                foreach ($cart as $key => $params) {
                    $book = $em->getRepository('AppBundle:Book')->find($params['bookId']);
                    if ($book) {
                        $userbook_current = $em->getRepository('AppBundle:UsersBook')
                            ->findOneBy(['user' => $this->getUser(), 'book' => $book]);
                        if($params['userBookId'] > 0)
                        {
                            $seller_userbook = $em->getRepository('AppBundle:UsersBook')
                                ->find($params['userBookId']);
                            $seller = $seller_userbook->getUser();
                            $current_price = $seller_userbook->getPrice();
                            $item_total_price = $params['qty'] * $current_price;
                            $seller->setCash($seller->getCash() + $item_total_price);
                            $total += $item_total_price;
                            if($params['qty'] == $seller_userbook->getQty())
                            {
                                $em->remove($seller_userbook);
                            }
                            else
                            {
                                $seller_userbook->setQty($seller_userbook->getQty() - $params['qty']);
                                $em->persist($seller_userbook);
                            }
                            $em->flush();
                        }
                        else
                        {
                            $book = Promotions::apply_promotion($em, $this->getUser(), $book);
                            $current_price = $book->promo_price;
                            $total += $params['qty'] * $current_price;
                        }
                        if ($userbook_current) {
                            $userbook_current->setQty($userbook_current->getQty() + $params['qty']);
                            $em->persist($userbook_current);
                            $em->flush();
                        } else {
                            $userbook = new UsersBook();
                            $userbook->setPrice($current_price);
                            $userbook->setUser($this->getUser());
                            $userbook->setBook($book);
                            $userbook->setQty($params['qty']);
                            $userbook->setSelling(false);
                            $em->persist($userbook);
                            $em->flush();
                        }

                        $book->setQuantity($book->getQuantity() - $params['qty']);
                        $em->persist($book);
                        $em->flush();
                    }
                }

                $user = $this->getUser();
                $user->setCash($user->getCash() - $total);
                $em->persist($user);
                $em->flush();

                $session->getFlashBag()->add('order_success', true);
                $cart = [];
                $session->set('cart', []);
            }
        }
        $cart_items = [];
        foreach ($cart as $key => $params) {
            $parsed_book = [];
            $book = $em->getRepository('AppBundle:Book')->find($params['bookId']);
            $parsed_book['id'] = $book->getId();
            $parsed_book['key'] = $key;
            $parsed_book['title'] = $book->getTitle();
            $parsed_book['quantity'] = $params['qty'];
            if ($params['userBookId'] > 0) {
                $parsed_book['price'] = $em->getRepository('AppBundle:UsersBook')
                    ->find($params['userBookId'])->getPrice();
                $parsed_book['title'] .= ' /втора употреба/';
            } else {
                $book = Promotions::apply_promotion($em, $this->getUser(), $book);
                $parsed_book['price'] = $book->promo_price;
            }
            $cart_items[] = $parsed_book;
        }

        return $this->render('cart/cart.html.twig', array(
                'cart' => $cart_items,
                'cash' => $this->getUser()->getCash(),
                'menu' => 'cart',
                'user' => $this->getUser(),
                'single_page' => true
            ) + $status);
    }

    /**
     * Validates if we have enough quantity to sell.
     *
     * @param Array $cart Cart keys with parameters
     *
     * @return boolean
     */
    private function validate_quantity(Array $cart)
    {
        $em = $this->getDoctrine()->getManager();
        foreach ($cart as $key => $params) {
            if ($params['userBookId'] > 0) {
                $book_qty = $em->getRepository('AppBundle:UsersBook')->find($params['userBookId'])->getQty();
            } else {
                $book_qty = $em->getRepository('AppBundle:Book')->find($params['bookId'])->getQuantity();
            }
            if ($book_qty < $params['qty'] || $params['qty'] > 10) {
                return false;
            }
        }
        return true;
    }

    /**
     * Validates if we have enough cash to pay the order.
     *
     * @param Array $cart Cart keys with parameters
     *
     * @return boolean
     */
    private function validate_cash(Array $cart)
    {
        $em = $this->getDoctrine()->getManager();
        $total = 0;
        foreach ($cart as $key => $params) {
            if ($params['userBookId'] > 0) {
                $price = $em->getRepository('AppBundle:UsersBook')->find($params['userBookId'])->getPrice();
            } else {
                $price = $em->getRepository('AppBundle:Book')->find($params['bookId'])->getPrice();
            }
            $total += $params['qty'] * $price;
        }
        return $this->getUser()->getCash() >= $total;
    }
}
