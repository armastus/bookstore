var toggler = '.navbar-toggle';
var pagewrapper = '#page-content';
var navigationwrapper = '.navbar-header';
var size_switch_width = 580;
var slide_sizes = {
  'small': {
    'menuwidth': '100%',
    'slidewidth': '80%',
    'menuneg': '-100%',
    'slideneg': '-80%',
  },
  'large': {
    'menuwidth': '100%',
    'slidewidth': '60%',
    'menuneg': '-100%',
    'slideneg': '-60%',
  }
};
var selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';
var widget_menu_location = 'normal';
function disableBodyScroll() {
  $('html,#page').addClass("noScroll");
}
function enableBodyScroll() {
  $('html,#page').removeClass("noScroll");
}
function viewport() {
  var e = window, a = 'inner';
  if (!('innerWidth' in window )) {
    a = 'client';
    e = document.documentElement || document.body;
  }
  return { width : e[ a+'Width' ] , height : e[ a+'Height' ] };
}
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}
function toggleMobileNav()
{
    var viewport_width = viewport().width;
    if(viewport_width < size_switch_width)
    {
        var slide_size = slide_sizes['small'];
    }
    else
    {
        var slide_size = slide_sizes['large'];
    }
    var selected = $(toggler).hasClass('slide-active');
    if(selected) {
        enableBodyScroll();
        $('#mobile-nav-bg').hide();
    }
    else
    {
        disableBodyScroll();
        $('#mobile-nav-bg').show();
    }
    $('#slidemenu').stop().animate({
        left: selected ? slide_size['menuneg'] : '0px'
    });
    $('#navbar-height-col').stop().animate({
        left: selected ? slide_size['slideneg'] : '0px'
    });
    $(pagewrapper).stop().animate({
        left: selected ? '0px' : slide_size['slidewidth']
    });
    $(navigationwrapper).stop().animate({
        left: selected ? '0px' : slide_size['slidewidth']
    });
    $(toggler).toggleClass('slide-active', !selected);
    $('#slidemenu').toggleClass('slide-active');
    $('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');
    $(window).resize();
}
$(document).ready(function () {
    /*
    $("#mobile-nav-bg").swipe( {
        tap:function(event, target) {
            if($(toggler).hasClass('slide-active'))
            {
                toggleMobileNav();
            }
        }
    });
    $("html").swipe( {
        swipeLeft:function(event, direction, distance, duration, fingerCount, fingerData) {
            var viewport_width = viewport().width;
            if($(toggler).hasClass('slide-active') && viewport_width <= 840)
            {
                setTimeout(function(){ toggleMobileNav(); }, 100);
            }
        },
        swipeRight:function(event, direction, distance, duration, fingerCount, fingerData) {
            var viewport_width = viewport().width;
            if( ! $(toggler).hasClass('slide-active') && viewport_width <= 840)
            {
                setTimeout(function(){ toggleMobileNav(); }, 100);
            }
        },
        threshold:75
    });
    */
    History.Adapter.bind(window,'statechange',function(){
        // Log the State
        var State = History.getState();
        if(last_url != State.url)
        {
            var state_url = State.url;
            var new_cat_url = state_url.substring(state_url.lastIndexOf('/') + 1).split('?')[0];
            sort = getParameterByName("sort", state_url);
            search = getParameterByName("search", state_url);
            secs = getParameterByName("sections", state_url);
            if(secs == null) secs = ''; 
            if(sort == null) sort = 'newest'; 
            if(search == null) search = ''; 
            if(new_cat_url == null || new_cat_url == '') new_cat_url = 'all';
            set_category(new_cat_url);
            set_sections();
            set_sort();
            set_search();
            if(search=='') search="-";
            change_url(true);
        }
    });

/****************BEGIN MOBILE SIDEBAR CODE****************/

    //stick in the fixed 100% height behind the navbar but don't wrap it
    $('#slide-nav.navbar-inverse').after($('<div class="inverse" id="navbar-height-col"></div>'));

    $('#slide-nav.navbar-default').after($('<div id="navbar-height-col"></div>'));  
    // Enter your ids or classes
    
    $("#slide-nav").on("click", toggler, function (e) {
        toggleMobileNav();
    });
    
    $(window).on("resize", function () {
      var viewport_width = viewport().width;
      $('#colophon').css('width', viewport_width);
      $('#page').css('min-height', viewport().height);
      var slide_active = $("#slide-nav").hasClass('slide-active');

      if(viewport_width < size_switch_width)
      {
        $('#slidemenu, #navbar-height-col').css('width', '80%');
        if($(".navbar-toggle").hasClass('slide-active'))
        {
          $(".navbar-header").css('left', '80%');
        }
        $('#cart_icon').show();
      }
      else if(viewport_width > 840)
      {
        $('#slidemenu').css('width', 'initial');
        $('#cart_icon').hide();
      }
      else
      {
        $('#slidemenu, #navbar-height-col').css('width', '60%');
        if($(".navbar-toggle").hasClass('slide-active'))
        {
          $(".navbar-header").css('left', '60%');
        }
        $('#cart_icon').show();
      }

      if (viewport_width > 840)
      {
        if(widget_menu_location != 'normal')
        {
          $('#slide_options').contents().appendTo('#sidebar_options');
          $('#slide_options').hide();
          widget_menu_location = 'normal';
        }
        $('#slidemenu').css('height', 'auto');
        var selected = $(this).hasClass('slide-active');

        enableBodyScroll();
      }
      else
      {
        if(widget_menu_location != 'sidebar')
        {
          $('#sidebar_options').contents().appendTo('#slide_options');
          $('#slide_options').show();
          widget_menu_location = 'sidebar';
        }
        $('#slidemenu').css('height', viewport().height);

        if($('#slide-nav').hasClass('slide-active'))
        {
          disableBodyScroll();
        }  
      }
      if(viewport_width > 840 && $("#slide-nav").hasClass('slide-active')) {
        $("#slide-nav").click();
      }
      if (viewport_width > 840 && $('.navbar-toggle').is(':hidden')) {
        $(selected).removeClass('slide-active');
      }
    });
    $(window).resize();
/****************END MOBILE SIDEBAR CODE****************/
});