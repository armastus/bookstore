<?php

namespace AppBundle\Logic;

use AppBundle\Entity\Book;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;

/**
 * Promotions controller.
 *
 */
class Promotions
{
    public static $active_types = [
        'global' => 'Всички продукти',
        'product' => 'Продукт',
        'category' => 'Категория',
        'user_older_than' => 'Дата на регистрация',
        'user_cash_greater_than' => 'Виртуален портфейл'
    ];
    /**
     * Apply promotion with highest discount
     *
     * @param EntityManager $em
     * @param User $user || null
     * @param Book $book
     *
     * @return Book $book
     */
    public static function apply_promotion(EntityManager $em, $user, Book $book)
    {
        $qb = $em->createQueryBuilder();
        $qb->from('AppBundle:Promotion', 'p')
            ->select('p')
            ->andWhere('CURRENT_TIMESTAMP() BETWEEN p.start AND p.end')
            ->andWhere('p.active=1');
        $promotions = $qb->getQuery()->getResult();
        $discounts = [0];
        foreach ($promotions as $promotion) {
            switch ($promotion->getType()) {
                case 'global':
                    $discounts[] = $promotion->getDiscount();
                    break;
                case 'product':
                    if ($promotion->getBook()->getId() == $book->getId())
                        $discounts[] = $promotion->getDiscount();
                    break;
                case 'category':
                    if ($promotion->getCategory()->getId() == $book->getCategory()->getId())
                        $discounts[] = $promotion->getDiscount();
                    break;
                case 'user_older_than':
                    if ($user && $user->getCreatedAt()->getTimestamp() <= strtotime('-' . $promotion->getDays() . ' days'))
                        $discounts[] = $promotion->getDiscount();
                    break;
                case 'user_cash_greater_than':
                    if ($user && $user->getCash() >= $promotion->getCash())
                        $discounts[] = $promotion->getDiscount();
                    break;
            }
        }
        $max_discount = max($discounts);
        if ($max_discount > 0) {
            $book->promo_price = number_format($book->getPrice() * (100 - $max_discount) / 100, 2, '.', '');
            $book->discount = $max_discount;
        } else {
            $book->promo_price = $book->getPrice();
        }

        return $book;
    }
}
