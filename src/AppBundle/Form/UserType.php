<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', TextType::class, [
            'required' =>  false,
            'label' => 'Потр. име'
        ])
                ->add('email', TextType::class,[
                'required' =>  false,
                'label' => 'Имейл'
        ])
                ->add('password_raw', RepeatedType::class, [
                    'type' => PasswordType::class,
                    'required' =>  false,
                    'first_options' => [
                        'label'=> 'Парола',
                    ],
                    'second_options' =>[
                        'label'=> 'Повтори парола'
                    ]
                ]);
        if($options['access'] == 'full')
        {
            $builder->add('cash', MoneyType::class, ['currency' => 'BGN', 'label' => 'Налична сума']);
            if( ! $options['hide_role_field'])
            {
                $builder->add('role', ChoiceType::class, [
                    'choices' => ['Потребител' => 'ROLE_USER', 'Редактор' => 'ROLE_EDITOR', 'Администратор' => 'ROLE_ADMIN'],
                    'label' => 'Роля'
                ]);
            }
        }
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' =>  User::class,
            'access' => 'limited',
            'hide_role_field' => false
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_user';
    }


}
