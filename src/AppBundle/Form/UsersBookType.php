<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\Ent;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Category;

class UsersBookType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
            if($options['access'] == 'full')
            {
                $builder->add('book', EntityType::class, [
                    'class' => 'AppBundle:Book',
                    'choice_label' => 'title',
                    'label' => 'Книга'])
                    ->add('qty', IntegerType::class, ['label' => 'Количество']);
            }

            $builder->add('selling', ChoiceType::class, [
                    'choices' => ['Да' => 1, 'Не' => 0],
                    'label' => 'В продажба'
                ])
                ->add('price', MoneyType::class, ['currency' => 'BGN', 'label' => 'Продажна Цена']);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\UsersBook',
            'access' => 'full'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_usersbook';
    }
}
