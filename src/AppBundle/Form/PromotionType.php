<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\Ent;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Category;

class PromotionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('start', DateTimeType::class, ['label' => 'Начало на промоцията'])
            ->add('end', DateTimeType::class, ['label' => 'Край на промоцията'])
            ->add('active', ChoiceType::class, [
                'choices' => ['Да' => 1, 'Не' => 0],
                'label' => 'Активна'
            ])
            ->add('discount', IntegerType::class, ['label' => 'Отстъпка']);

        switch ($options['type']) {
            case 'product':
                $builder->add('book', EntityType::class, [
                    'class' => 'AppBundle:Book',
                    'choice_label' => 'title',
                    'label' => 'Книга']);
                break;
            case 'category':
                $builder->add('category', EntityType::class, [
                    'class' => 'AppBundle:Category',
                    'choice_label' => 'name',
                    'label' => 'Категория']);
                break;
            case 'user_older_than':
                $builder->add('days', IntegerType::class, ['label' => 'Потребител регистриран преди повече от X дни']);
                break;
            case 'user_cash_greater_than':
                $builder->add('cash', MoneyType::class, ['currency' => 'BGN', 'label' => 'Потребител с виртуален портфейл повече от X лв']);
                break;
            default:
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Promotion',
            'type' => 'global'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_promotion';
    }
}
