<?php

namespace AppBundle\Form;

use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\Ent;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Intl\Intl;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Category;

class BookType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if($options['access'] == 'limited')
        {
            $builder->add('quantity', IntegerType::class, ['label' => 'Налично количество'])
                ->add('category', EntityType::class, [
                    'class' => 'AppBundle:Category',
                    'choice_label' => 'name',
                    'label' => 'Категория'])
                ->add('sort', IntegerType::class, ['label' => 'Сортиране']);
        }
        else
        {
            $builder->add('title', null, ['label' => 'Заглавие'])
                ->add('visible', ChoiceType::class, [
                    'choices' => ['Да' => 1, 'Не' => 0],
                    'label' => 'Видима'
                ])
                ->add('description', TextareaType::class, ['label' => 'Описание'])
                ->add('author', null, ['label' => 'Автор'])
                ->add('quantity', IntegerType::class, ['label' => 'Налично количество'])
                ->add('price', MoneyType::class, ['currency' => 'BGN', 'label' => 'Цена'])
                ->add('category', EntityType::class, [
                    'class' => 'AppBundle:Category',
                    'choice_label' => 'name',
                    'label' => 'Категория'])
                ->add('sort', IntegerType::class, ['label' => 'Сортиране']);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Book',
            'access' => 'full'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_book';
    }
}
