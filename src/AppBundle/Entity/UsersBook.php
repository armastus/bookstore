<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Project
 *
 * @ORM\Table(name="users_books")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UsersBookRepository")
 */
class UsersBook
{
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Book", inversedBy="booksUser")
     * @ORM\JoinColumn(name="book_id", referencedColumnName="id", nullable=false)
     */
    protected $book;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="booksUser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;
    /**
     * @var int
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * @var int
     *
     * @ORM\Column(name="qty", type="integer")
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value=-1, message="The quantity should be a positive number")
     */
    private $qty;
    /**
     * @var bool
     *
     * @ORM\Column(name="selling", type="boolean")
     * @Assert\NotBlank()
     */
    private $selling;
    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", length=10,  precision=2)
     * @Assert\NotBlank()
     * @Assert\GreaterThan(value=0, message="The price should be greater than zero")
     */
    private $price;

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getQty()
    {
        return $this->qty;
    }

    /**
     * @param int $qty
     */
    public function setQty($qty)
    {
        $this->qty = $qty;
    }

    /**
     * @return bool
     */
    public function isSelling()
    {
        return $this->selling;
    }

    /**
     * @param bool $selling
     */
    public function setSelling($selling)
    {
        $this->selling = $selling;
    }

    /**
     * @return mixed
     */
    public function getBook()
    {
        return $this->book;
    }

    /**
     * @param mixed $book
     */
    public function setBook($book)
    {
        $this->book = $book;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}

