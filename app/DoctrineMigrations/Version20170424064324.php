<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170424064324 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `user` ADD `cash` DECIMAL(10,2) NOT NULL AFTER `role`;');
        $this->addSql('CREATE TABLE `books` (
                              `id` int(11) NOT NULL,
                              `category_id` int(11) NOT NULL,
                              `visible` int(11) NOT NULL,
                              `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                              `updated_at` TIMESTAMP on update CURRENT_TIMESTAMP NULL DEFAULT NULL,
                              `deleted_at` TIMESTAMP NULL DEFAULT NULL,
                              `title` int(11) NOT NULL,
                              `description` int(11) NOT NULL,
                              `quantity` int(11) NOT NULL,
                              `price` int(11) NOT NULL,
                              `author` int(11) NOT NULL,
                              `sort` int(11) NOT NULL
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;');
        $this->addSql('ALTER TABLE `books`
                              ADD PRIMARY KEY (`id`),
                              ADD KEY `category_id` (`category_id`),
                              ADD KEY `visible` (`visible`),
                              ADD KEY `sort` (`sort`);');
        $this->addSql('ALTER TABLE `books`
                              MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `user` DROP `cash`;');
        $this->addSql('DROP TABLE `books`;');

    }
}
