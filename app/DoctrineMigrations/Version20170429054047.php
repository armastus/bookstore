<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170429054047 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("CREATE TABLE `promotions` (
                            `id` int(11) NOT NULL,
                          `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                          `updated_at` timestamp NULL DEFAULT NULL,
                          `deleted_at` timestamp NULL DEFAULT NULL,
                          `type` enum('global','product','category','user_older_than','user_cash_greater_than') COLLATE utf8_unicode_ci NOT NULL,
                          `start` datetime DEFAULT NULL,
                          `end` datetime DEFAULT NULL,
                          `discount` TINYINT UNSIGNED NOT NULL,
                          `book_id` int(11) DEFAULT NULL,
                          `category_id` int(11) DEFAULT NULL,
                          `cash` DECIMAL(10,2) DEFAULT NULL,
                          `days` `days` INT(11) NULL DEFAULT '0',
                          `active` tinyint(1) NOT NULL DEFAULT '1'
                        ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;");


        $this->addSql("ALTER TABLE `promotions`
                          ADD PRIMARY KEY (`id`),
                          ADD KEY `start` (`start`,`end`);");

        $this->addSql("ALTER TABLE `promotions`
                            MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");

        $this->addSql("ALTER TABLE `promotions` ADD INDEX(`book_id`);");
        $this->addSql("ALTER TABLE `promotions` ADD INDEX(`category_id`);");

        $this->addSql("ALTER TABLE `promotions` ADD FOREIGN KEY (`book_id`) REFERENCES `bookstore`.`books`(`id`) ON DELETE CASCADE;");
        $this->addSql("ALTER TABLE `promotions` ADD FOREIGN KEY (`category_id`) REFERENCES `bookstore`.`categories`(`id`) ON DELETE CASCADE;");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `promotions`');
    }
}
