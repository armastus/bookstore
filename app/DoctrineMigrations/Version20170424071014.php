<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170424071014 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `users_books` (
                              `id` int(11) NOT NULL,
                              `user_id` int(11) NOT NULL,
                              `book_id` int(11) NOT NULL,
                              `qty` smallint(5) UNSIGNED NOT NULL,
                              `selling` tinyint(1) NOT NULL DEFAULT \'0\',
                              `price` DECIMAL(10,2) NOT NULL DEFAULT \'0\'
                            ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;');
        $this->addSql('ALTER TABLE `users_books`
                              ADD PRIMARY KEY (`id`),
                              ADD KEY `user_id` (`user_id`),
                              ADD KEY `book_id` (`book_id`);');
        $this->addSql('ALTER TABLE `users_books`
                              MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;');

        $this->addSql('ALTER TABLE `users_books` ADD FOREIGN KEY (`book_id`) REFERENCES `bookstore`.`books`(`id`) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE `users_books` ADD FOREIGN KEY (`user_id`) REFERENCES `bookstore`.`user`(`id`) ON DELETE CASCADE;');

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE `users_books`;');

    }
}
