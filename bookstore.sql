-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30 апр 2017 в 10:27
-- Версия на сървъра: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bookstore`
--

-- --------------------------------------------------------

--
-- Структура на таблица `books`
--

CREATE TABLE `books` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `visible` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `quantity` int(11) NOT NULL,
  `price` decimal(10,2) NOT NULL,
  `author` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `sort` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `books`
--

INSERT INTO `books` (`id`, `category_id`, `visible`, `created_at`, `updated_at`, `deleted_at`, `title`, `description`, `quantity`, `price`, `author`, `sort`) VALUES
(1, 4, 1, '2017-04-25 04:40:53', '2017-04-30 08:15:36', NULL, 'Книга 1', 'Примерно описание', 27, '15.90', 'Григор Иванов', 40),
(2, 2, 1, '2017-04-25 04:40:53', '2017-04-30 08:21:47', NULL, 'Книга 2', 'Примерно описание', 3, '12.00', 'Иван Григоров', 30),
(4, 2, 1, '2017-04-25 04:40:53', '2017-04-30 08:22:42', NULL, 'Книга 3', 'Примерно описание', 5, '7.90', 'Иван Григоров', 35);

-- --------------------------------------------------------

--
-- Структура на таблица `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `categories`
--

INSERT INTO `categories` (`id`, `created_at`, `updated_at`, `deleted_at`, `name`) VALUES
(1, '2017-04-29 11:01:26', '2017-04-29 10:39:22', NULL, 'Астрология'),
(2, '2017-04-30 06:00:06', NULL, NULL, 'История'),
(4, '2017-04-29 10:31:33', '2017-04-29 10:31:33', NULL, 'Романи');

-- --------------------------------------------------------

--
-- Структура на таблица `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20170411160149'),
('20170412114605'),
('20170412140655'),
('20170412141921'),
('20170424064324'),
('20170424070504'),
('20170424071014'),
('20170425045759'),
('20170425050819'),
('20170426052525'),
('20170429054047');

-- --------------------------------------------------------

--
-- Структура на таблица `promotions`
--

CREATE TABLE `promotions` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `type` enum('global','product','category','user_older_than','user_cash_greater_than') COLLATE utf8_unicode_ci NOT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `book_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `cash` decimal(10,2) DEFAULT NULL,
  `days` int(11) DEFAULT '0',
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `discount` tinyint(3) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `promotions`
--

INSERT INTO `promotions` (`id`, `created_at`, `updated_at`, `deleted_at`, `type`, `start`, `end`, `book_id`, `category_id`, `cash`, `days`, `active`, `discount`) VALUES
(2, '2017-04-29 07:37:30', '2017-04-30 05:31:11', NULL, 'product', '2017-04-28 00:00:00', '2017-04-30 23:05:00', 1, NULL, '0.00', 0, 1, 60),
(3, '2017-04-29 07:40:14', '2017-04-30 05:32:56', NULL, 'category', '2017-04-28 00:00:00', '2017-04-30 22:00:00', NULL, 4, '0.00', 0, 1, 70),
(4, '2017-04-29 07:48:00', '2017-04-30 05:33:39', NULL, 'user_older_than', '2017-04-01 00:00:00', '2017-05-31 23:00:00', NULL, NULL, '0.00', 2, 1, 75),
(5, '2017-04-29 07:54:18', '2017-04-30 05:34:17', NULL, 'user_cash_greater_than', '2017-04-30 00:00:00', '2017-05-03 00:00:00', NULL, NULL, '190.00', 0, 0, 85),
(6, '2017-04-30 06:00:31', '2017-04-30 06:00:31', NULL, 'user_older_than', '2012-01-01 00:00:00', '2012-01-01 00:00:00', NULL, NULL, NULL, 5, 1, 10),
(7, '2017-04-30 06:03:18', '2017-04-30 06:03:18', NULL, 'global', '2012-01-01 00:00:00', '2012-01-01 00:00:00', NULL, NULL, NULL, NULL, 1, 50);

-- --------------------------------------------------------

--
-- Структура на таблица `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cash` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `user`
--

INSERT INTO `user` (`id`, `created_at`, `updated_at`, `deleted_at`, `username`, `email`, `password`, `role`, `cash`) VALUES
(1, '2017-04-13 07:17:05', '2017-04-30 07:08:22', NULL, 'editor', 'ashmodean7@gmail.com', '$2y$13$9sKVFx6.x242qjU7unyjouIBIc/vXCDumOCm/C9EKcH/1Avy8lGfW', 'ROLE_EDITOR', '131.00'),
(3, '2017-04-28 07:17:05', '2017-04-30 07:19:11', NULL, 'admin', 'ashmodean10@gmail.com', '$2y$13$I6sp9ICiDmgB7qCcxwitO.axVtgOyxUv4Ta7aUsXAIhqZFqSnpnZy', 'ROLE_ADMIN', '60.71'),
(5, '2017-04-30 08:12:14', NULL, NULL, 'user', 'user@test.com', '$2y$13$GixpT9e6BVyjkQf55SOnkOnF4ILjw/IKAUjiUYLnXoMYrt6/yOBS.', 'ROLE_USER', '100.00');

-- --------------------------------------------------------

--
-- Структура на таблица `users_books`
--

CREATE TABLE `users_books` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `qty` smallint(5) UNSIGNED NOT NULL,
  `selling` tinyint(1) NOT NULL DEFAULT '0',
  `price` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Схема на данните от таблица `users_books`
--

INSERT INTO `users_books` (`id`, `user_id`, `book_id`, `qty`, `selling`, `price`) VALUES
(2, 1, 2, 11, 1, '10.00'),
(4, 3, 2, 7, 1, '15.00'),
(6, 3, 1, 3, 0, '7.00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `books`
--
ALTER TABLE `books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `visible` (`visible`),
  ADD KEY `sort` (`sort`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `promotions`
--
ALTER TABLE `promotions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `start` (`start`,`end`),
  ADD KEY `book_id` (`book_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_8D93D649F85E0677` (`username`),
  ADD UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`);

--
-- Indexes for table `users_books`
--
ALTER TABLE `users_books`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `book_id` (`book_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `books`
--
ALTER TABLE `books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `promotions`
--
ALTER TABLE `promotions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `users_books`
--
ALTER TABLE `users_books`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Ограничения за дъмпнати таблици
--

--
-- Ограничения за таблица `books`
--
ALTER TABLE `books`
  ADD CONSTRAINT `books_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `promotions`
--
ALTER TABLE `promotions`
  ADD CONSTRAINT `promotions_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `promotions_ibfk_2` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;

--
-- Ограничения за таблица `users_books`
--
ALTER TABLE `users_books`
  ADD CONSTRAINT `users_books_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `books` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_books_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
